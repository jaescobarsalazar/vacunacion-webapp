/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAowIywwv2UFTM89KTOUBY7YQegS9dbyqE",
    authDomain: "vacunacion-66a40.firebaseapp.com",
    projectId: "vacunacion-66a40",
    storageBucket: "vacunacion-66a40.appspot.com",
    messagingSenderId: "297889717570",
    appId: "1:297889717570:web:1dc771f9c94104c52f3cdb"
  }
};
