import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonasVacunadas } from '../model/personas-vacunadas';

@Injectable({
  providedIn: 'root'
})
export class PersonasVacunadasService {

  coleccionPersonasVacunadas: AngularFirestoreCollection<PersonasVacunadas>;
  personasVacunadas: Observable<PersonasVacunadas[]>;
  personaVacunadaDoc: AngularFirestoreDocument<PersonasVacunadas>;

  constructor(public db: AngularFirestore) {
    this.coleccionPersonasVacunadas = this.db.collection('personasVacunadas');
    this.personasVacunadas = this.coleccionPersonasVacunadas.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as PersonasVacunadas;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerPersonasVacunada() {
    return this.personasVacunadas;
  }

  agregarPersonaVacunada(personaVacunada: PersonasVacunadas) {
    this.coleccionPersonasVacunadas.add(personaVacunada);
  }

  borrarPersonaVacunada(personaVacunada: PersonasVacunadas) {
    this.personaVacunadaDoc = this.db.doc(`personasVacunadas/${personaVacunada.id}`);
    this.personaVacunadaDoc.delete();
  }

  actualizarPersonaVacunada(personaVacunada: PersonasVacunadas) {
    this.personaVacunadaDoc = this.db.doc(`personasVacunadas/${personaVacunada.id}`);
    this.personaVacunadaDoc.update(personaVacunada);
  }

}
