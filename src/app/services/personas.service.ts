import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Persona } from '../model/personas';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  coleccionPersonas: AngularFirestoreCollection<Persona>;
  personas: Observable<Persona[]>;
  personaDoc: AngularFirestoreDocument<Persona>;

  constructor(public db: AngularFirestore) {
    this.coleccionPersonas = this.db.collection('personas');
    this.personas = this.coleccionPersonas.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Persona;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerPersonas() {
    return this.personas;
  }

  agregarPersona(persona: Persona) {
    this.coleccionPersonas.add(persona);
  }

  borrarPersona(persona: Persona) {
    this.personaDoc = this.db.doc(`personas/${persona.id}`);
    this.personaDoc.delete();
  }

  actualizarPersona(persona: Persona) {
    this.personaDoc = this.db.doc(`personas/${persona.id}`);
    this.personaDoc.update(persona);
  }

}
