import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Vacuna } from '../model/vacunas';

@Injectable({
  providedIn: 'root'
})
export class VacunasService {

  coleccionVacunas: AngularFirestoreCollection<Vacuna>;
  vacunas: Observable<Vacuna[]>;
  vacunaDoc: AngularFirestoreDocument<Vacuna>;

  constructor(public db: AngularFirestore) {
    this.coleccionVacunas = this.db.collection('vacunas');
    this.vacunas = this.coleccionVacunas.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Vacuna;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerVacunas() {
    return this.vacunas;
  }

  agregarVacuna(persona: Vacuna) {
    this.coleccionVacunas.add(persona);
  }

  borrarVacunas(persona: Vacuna) {
    this.vacunaDoc = this.db.doc(`vacunas/${persona.id}`);
    this.vacunaDoc.delete();
  }

  actualizarVacuna(persona: Vacuna) {
    this.vacunaDoc = this.db.doc(`vacunas/${persona.id}`);
    this.vacunaDoc.update(persona);
  }

}
