import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PersonalSalud } from '../model/personal-salud';


@Injectable({
  providedIn: 'root'
})
export class PersonalSaludService {

  coleccionPersonalSalud: AngularFirestoreCollection<PersonalSalud>;
  personalSalud: Observable<PersonalSalud[]>;
  personalSaludDoc: AngularFirestoreDocument<PersonalSalud>;

  constructor(public db: AngularFirestore) {
    this.coleccionPersonalSalud = this.db.collection('personalSalud');
    this.personalSalud = this.coleccionPersonalSalud.snapshotChanges().pipe(map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as PersonalSalud;
        data.id = a.payload.doc.id;
         return data;
      });
    }));
  }

  obtenerPersonalSalud() {
    return this.personalSalud;
  }

  agregarPersonalSalud(personalSalud: PersonalSalud) {
    this.coleccionPersonalSalud.add(personalSalud);
  }

  borrarPersonalSalud(personalSalud: PersonalSalud) {
    this.personalSaludDoc = this.db.doc(`personalSalud/${personalSalud.id}`);
    this.personalSaludDoc.delete();
  }

  actualizarPersonalSalud(personalSalud: PersonalSalud) {
    this.personalSaludDoc = this.db.doc(`personalSalud/${personalSalud.id}`);
    this.personalSaludDoc.update(personalSalud);
  }

}
