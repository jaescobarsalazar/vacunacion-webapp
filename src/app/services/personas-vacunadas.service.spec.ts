import { TestBed } from '@angular/core/testing';

import { PersonasVacunadasService } from './personas-vacunadas.service';

describe('PersonasVacunadasService', () => {
  let service: PersonasVacunadasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonasVacunadasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
