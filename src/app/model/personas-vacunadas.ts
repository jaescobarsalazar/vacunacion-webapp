import { PersonalSalud } from "./personal-salud";
import { Persona } from "./personas";
import { Vacuna } from "./vacunas";

export interface PersonasVacunadas {
    id?: string;
    personaVacunada?:Persona;
    profesionalSalud?:PersonalSalud;
    vacuna?:Vacuna;
    fecha?: string;
  }
