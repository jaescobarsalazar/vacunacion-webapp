export interface PersonalSalud {
    id?: string;
    nombre?: string;
    rut?: string;
    especialidad?: string;
    fono?: string;
    email?: string;
  }
