export interface Persona {
    id?: string;
    nombre?: string;
    apellidoPaterno?: string;
    apellidoMaterno?: string;
    rut?: string;
    fono?: string;
    email?: string;
    fechaNacimiento?: string;
  }
  