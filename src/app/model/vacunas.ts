export interface Vacuna {
  id?: string;
  nombre?: string;
  tipo?: string;
  dosis?: string;
  periodoEnDias?: string;
}
