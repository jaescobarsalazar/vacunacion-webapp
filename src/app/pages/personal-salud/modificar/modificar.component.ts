import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService } from "@nebular/theme";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PersonalSalud } from "../../../model/personal-salud";
import { Persona } from "../../../model/personas";
import { PersonasVacunadas } from "../../../model/personas-vacunadas";
import { PersonalSaludService } from "../../../services/personal-salud.service";
import { PersonasService } from "../../../services/personas.service";


@Component({
  selector: "ngx-modificar",
  templateUrl: "./modificar.component.html",
  styleUrls: ["./modificar.component.scss"],
})
export class ModificarComponent implements OnInit {
  
@Input() personal : PersonalSalud;

  formulario: FormGroup;
 

  constructor(
    private fb: FormBuilder,
    public personalSaludServices: PersonalSaludService,
    private router: Router,
    private modalService: NgbModal,
    private toastrService: NbToastrService
  ) {}

  mostrarNotificacion( status, mensaje,duration
  ) {
  this.toastrService.show(
    status || mensaje,
    ` ${mensaje}`,
    { status,duration });
}
  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: new FormControl(this.personal.nombre),
      rut: new FormControl(this.personal.rut),
      especialidad: new FormControl(this.personal.especialidad),
      fono: new FormControl(this.personal.fono),
      email: new FormControl(this.personal.email)
    });
  }
  resetForm() {
    this.formulario.reset(this.formulario.value);
  }
  modificar() {
    this.personal.nombre = this.formulario.controls.nombre.value;
    this.personal.rut = this.formulario.controls.rut.value;
    this.personal.especialidad = this.formulario.controls.especialidad.value;
    this.personal.fono = this.formulario.controls.fono.value;
    this.personal.email = this.formulario.controls.email.value;
    console.log(this.personal)
    this.personalSaludServices.actualizarPersonalSalud(this.personal);
    this.mostrarNotificacion( "success", "Personal de Salud Registrado!",10000)
    this.cerrarModal();
  }

  cerrarModal() {
    this.modalService.dismissAll();

  }
}
