import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonalSaludRoutingModule } from './personal-salud-routing.module';
import { ModificarComponent } from './modificar/modificar.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { PersonalSaludComponent } from './personal-salud.component';
import { NbCardModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ModificarComponent,
    ListarComponent,
    AgregarComponent,
    PersonalSaludComponent
  ],
  imports: [
    CommonModule,
    PersonalSaludRoutingModule,
    NbCardModule,
    ReactiveFormsModule
  ],
  entryComponents:[AgregarComponent]
})
export class PersonalSaludModule { }
