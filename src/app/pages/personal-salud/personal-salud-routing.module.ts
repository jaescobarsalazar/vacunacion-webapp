import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { PersonalSaludComponent } from './personal-salud.component';

const routes: Routes = [{
  path: '',
  component: PersonalSaludComponent,
  children: [
    {
      path: 'listar',
      component:ListarComponent,
    },
    {
      path: '',
      component:ListarComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonalSaludRoutingModule { }
