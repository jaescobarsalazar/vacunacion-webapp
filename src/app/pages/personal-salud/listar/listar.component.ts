import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { NbSearchService, NbToastrService, NbWindowService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Persona } from '../../../model/personas';
import { PersonasService } from '../../../services/personas.service';
import { Vacuna } from '../../../model/vacunas';
import { VacunasService } from '../../../services/vacunas.service';
import { PersonalSalud } from '../../../model/personal-salud';
import { PersonalSaludService } from '../../../services/personal-salud.service';
@Component({
  selector: 'ngx-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {

   contador=0;
  personalSalud : PersonalSalud[] ;
  value:any;
  edicion: boolean = false;
  personalEnEdicion: PersonalSalud;
  index : number = 0;
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  submitted = false;
  closeResult: string;
  detalle: any;
  constructor(public personalSaludServices: PersonalSaludService,
    private windowService: NbWindowService,
    private searchService: NbSearchService,
    private toastrService: NbToastrService,
    private modalService: NgbModal,
    ) {
      this.searchService.onSearchSubmit()
      .subscribe((data: any) => {
        this.value = data.term;
        console.log("filtro")
        console.log(this.value)
      })

  }

   open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {

    }, (reason) => {

    });
  }

  notificacionPladem(){
    this.showToast('','',10000);
  }
  showToast( status, mensaje,duration
    ) {
    status='danger'
    mensaje='Personal salud Registrado!'
    this.index += 1;
    this.toastrService.show(
      status || mensaje,
      ` ${mensaje}`,
      { status,duration });
  }
  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Window content from template',
        context: {
          text: 'some text to pass into template',
        },
      },
    );
  }

  ngOnInit() {
    this.personalSaludServices.obtenerPersonalSalud().subscribe(datos => {
      this.personalSalud = datos;
        this.contador=this.personalSalud.length;
      //llamamos al servicio y obtenemos el listado de personas
    });
  }


validarEliminar(vacuna, modal) {
  this.personalEnEdicion = vacuna;
  this.open(modal);
}


cargarModificar(personal: PersonalSalud, modal) {
  //cargamos los datos de la vacuna a modificar
  this.personalEnEdicion = personal;
  //se abre la modal
  this.open(modal);
}

eliminar() {
  this.personalSaludServices.borrarPersonalSalud(this.personalEnEdicion);
    this.showToast('danger','Personal de salud eliminado',3000)
   this.cerrarModal()
}
 AbrirModal(content, dato) {
    this.detalle = dato;
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }
  cerrarModal() {
    this.modalService.dismissAll();

  }
}
