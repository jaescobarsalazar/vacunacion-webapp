import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService } from "@nebular/theme";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PersonalSalud } from "../../../model/personal-salud";
import { Persona } from "../../../model/personas";
import { PersonasVacunadas } from "../../../model/personas-vacunadas";
import { PersonalSaludService } from "../../../services/personal-salud.service";
import { PersonasService } from "../../../services/personas.service";


@Component({
  selector: "ngx-agregar",
  templateUrl: "./agregar.component.html",
  styleUrls: ["./agregar.component.scss"],
})
export class AgregarComponent implements OnInit {
  formulario: FormGroup;
  personalSalud: PersonalSalud = {
    id: "",
    nombre: "",
    rut: "",
    especialidad: "",
    fono: "",
    email: ""
  };

  constructor(
    private fb: FormBuilder,
    public personalSaludServices: PersonalSaludService,
    private router: Router,
    private modalService: NgbModal,
    private toastrService: NbToastrService
  ) {}

  mostrarNotificacion( status, mensaje,duration
  ) {
  this.toastrService.show(
    status || mensaje,
    ` ${mensaje}`,
    { status,duration });
}
  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: new FormControl(""),
      rut: new FormControl(""),
      especialidad: new FormControl(""),
      fono: new FormControl(""),
      email: new FormControl("")
    });
  }
  resetForm() {
    this.formulario.reset(this.formulario.value);
  }
  guardar() {
    this.personalSalud.nombre = this.formulario.controls.nombre.value;
    this.personalSalud.rut = this.formulario.controls.rut.value;
    this.personalSalud.especialidad = this.formulario.controls.especialidad.value;
    this.personalSalud.fono = this.formulario.controls.fono.value;
    this.personalSalud.email = this.formulario.controls.email.value;
    this.personalSaludServices.agregarPersonalSalud(this.personalSalud);
    this.mostrarNotificacion( "success", "Personal de Salud Registrado!",10000)
    this.cerrarModal();
  }

  cerrarModal() {
    this.modalService.dismissAll();

  }
}
