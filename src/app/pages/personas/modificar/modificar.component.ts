import { Component, Input, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService } from "@nebular/theme";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Persona } from "../../../model/personas";
import { PersonasService } from "../../../services/personas.service";


@Component({
  selector: "ngx-modificar",
  templateUrl: "./modificar.component.html",
  styleUrls: ["./modificar.component.scss"],
})
export class ModificarComponent implements OnInit {
  formulario: FormGroup;
  @Input() persona : Persona;

  constructor(
    private fb: FormBuilder,
    public personasService: PersonasService,
    private router: Router,
    private modalService: NgbModal,
    private toastrService: NbToastrService
  ) {}

  mostrarNotificacion( status, mensaje,duration
  ) {
  this.toastrService.show(
    status || mensaje,
    ` ${mensaje}`,
    { status,duration });
}
  ngOnInit(): void {
    this.formulario = this.fb.group({
      id: new FormControl(""),
      nombre: new FormControl(""),
      apellidoMaterno: new FormControl(""),
      apellidoPaterno: new FormControl(""),
      fechaNacimiento: new FormControl(""),     
      rut: new FormControl(""),
      fono: new FormControl(""),
      email: new FormControl("")
    });
  }
  resetForm() {
    this.formulario.reset(this.formulario.value);
  }
  modificar() {
    this.persona.id = this.formulario.controls.id.value;
    this.persona.nombre = this.formulario.controls.nombre.value;
    this.persona.apellidoMaterno = this.formulario.controls.apellidoMaterno.value;
    this.persona.apellidoPaterno = this.formulario.controls.apellidoPaterno.value;
    this.persona.fechaNacimiento = this.formulario.controls.fechaNacimiento.value;
    this.persona.rut = this.formulario.controls.rut.value;
    this.persona.fono = this.formulario.controls.fono.value;
    this.persona.email = this.formulario.controls.email.value;
    this.personasService.agregarPersona(this.persona);
    this.mostrarNotificacion( "warning", "Persona modificada!",10000)
    this.cerrarModal();
  }

  cerrarModal() {
    this.modalService.dismissAll();

  }
}
