import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonasRoutingModule } from './personas-routing.module';
import { PersonasComponent } from './personas.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { ModificarComponent } from './modificar/modificar.component';
import { NbCardModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PersonasComponent,
    ListarComponent,
    AgregarComponent,
    ModificarComponent
  ],
  imports: [
    CommonModule,
    PersonasRoutingModule,
    NbCardModule,
    ReactiveFormsModule
  ],
  entryComponents:[AgregarComponent,ModificarComponent]
})
export class PersonasModule { }
