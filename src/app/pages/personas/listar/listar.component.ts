import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { NbSearchService, NbToastrService, NbWindowService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Persona } from '../../../model/personas';
import { PersonasService } from '../../../services/personas.service';
@Component({
  selector: 'ngx-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {
   contador=0;
  personas : Persona[] ;
  value:any;
  personasfiltradas : Persona[] ;
  personasBusqueda : Persona[];
  edicion: boolean = false;
  personaEnEdicion: Persona;
  index : number = 0;
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  submitted = false;
  closeResult: string;
  detalle: any;
  constructor(public personasService: PersonasService,
    private windowService: NbWindowService,
    private searchService: NbSearchService,
    private toastrService: NbToastrService,
    private modalService: NgbModal,
    ) {
      this.searchService.onSearchSubmit()
      .subscribe((data: any) => {
        this.value = data.term;
        console.log("filtro")
        console.log(this.value)
      })

  }

   open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {

    }, (reason) => {

    });
  }

  notificacionPladem(){
    this.showToast('','',10000);
  }
  showToast( status, mensaje,duration
    ) {
    status='danger'
    mensaje='Persona Registrada!'
    this.index += 1;
    this.toastrService.show(
      status || mensaje,
      ` ${mensaje}`,
      { status,duration });
  }
  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Window content from template',
        context: {
          text: 'some text to pass into template',
        },
      },
    );
  }

  ngOnInit() {
    this.personasService.obtenerPersonas().subscribe(datos => {
      this.personas = datos;
      this.contador=this.personas.length;
      //llamamos al servicio y obtenemos el listado de personas
    });


  }

  cargarModificar(persona: Persona, modal) {
    //cargamos los datos de la vacuna a modificar
    this.personaEnEdicion = persona;
    //se abre la modal
    this.open(modal);
  }
validarEliminar(persona, modal) {
  this.personaEnEdicion = persona;
  this.open(modal);
}
eliminar() {
    this.personasService.borrarPersona(this.personaEnEdicion);
    this.showToast('danger','Persona Eliminada',3000)
   
}

 AbrirModal(content, dato) {
    this.detalle = dato;
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }

}
