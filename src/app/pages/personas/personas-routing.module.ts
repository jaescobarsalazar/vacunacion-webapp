import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { PersonasComponent } from './personas.component';

const routes: Routes = [{
  path: '',
  component: PersonasComponent,
  children: [
    {
      path: 'listar',
      component:ListarComponent,
    },
    {
      path: '',
      component:ListarComponent,
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },

  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonasRoutingModule { }
