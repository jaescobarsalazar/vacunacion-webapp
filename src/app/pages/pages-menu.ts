import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Personas Vacunadas',
    icon: 'people-outline',
    link: '/pages/personas-vacunadas',
    home: true,
  },
  
  {
    title: 'Personas',
    icon: 'people-outline',
    link: '/pages/personas',
  },
  {
    title: 'PersonalDeSalud',
    icon: 'people-outline',
    link: '/pages/personal-salud',
  },
  {
    title: 'Vacunas',
    icon: 'people-outline',
    link: '/pages/vacunas'
  }
];
