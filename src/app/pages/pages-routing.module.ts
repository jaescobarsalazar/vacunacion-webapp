import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';


const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [

    {
      path: 'personas',
      loadChildren: () => import('./personas/personas.module')
        .then(m => m.PersonasModule),
    },
    {
      path: 'personal-salud',
      loadChildren: () => import('./personal-salud/personal-salud.module')
        .then(m => m.PersonalSaludModule),
    },
    {
      path: 'personas-vacunadas',
      loadChildren: () => import('./personas-vacunadas/personas-vacunadas.module')
        .then(m => m.PersonasVacunadasModule),
    },
    {
      path: 'vacunas',
      loadChildren: () => import('./vacunas/vacunas.module')
        .then(m => m.VacunasModule),
    },
    {
      path: '',
      redirectTo: 'personas-vacunadas',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
