import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { VacunasComponent } from './vacunas.component';

const routes: Routes = [{
  path: '',
  component: VacunasComponent,
  children: [
    {
      path: 'listar',
      component:ListarComponent,
    },
    {
      path: '',
      component:ListarComponent,
    },
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VacunasRoutingModule { }
