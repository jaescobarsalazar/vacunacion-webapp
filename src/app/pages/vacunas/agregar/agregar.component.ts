import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup, MaxValidator, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService } from "@nebular/theme";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Persona } from "../../../model/personas";
import { PersonasVacunadas } from "../../../model/personas-vacunadas";
import { Vacuna } from "../../../model/vacunas";
import { PersonalSaludService } from "../../../services/personal-salud.service";
import { PersonasService } from "../../../services/personas.service";
import { VacunasService } from "../../../services/vacunas.service";


@Component({
  selector: "ngx-agregar",
  templateUrl: "./agregar.component.html",
  styleUrls: ["./agregar.component.scss"],
})
export class AgregarComponent implements OnInit {
  formulario: FormGroup;
  controlForm=false;
  vacuna: Vacuna = {
    id: "",
    nombre: "",
    tipo: "",
    dosis: "",
    periodoEnDias:""
  };

  constructor(
    private fb: FormBuilder,
    public vacunaServices: VacunasService,
    private router: Router,
    private modalService: NgbModal,
    private toastrService: NbToastrService
  ) {}

  mostrarNotificacion( status, mensaje,duration
  ) {
  this.toastrService.show(
    status || mensaje,
    ` ${mensaje}`,
    { status,duration });
}
  ngOnInit(): void {
    this.formulario = this.fb.group({
      nombre: new FormControl("",
      [Validators.required,
        Validators.maxLength(30),
        ,Validators.minLength(3)]),
      tipo: new FormControl("",
      [Validators.required,
        Validators.maxLength(30),
        ,Validators.minLength(3)]),
      dosis: new FormControl("",
      [Validators.required,
        Validators.min(1),
        ,Validators.max(5)]),
      periodoEnDias: new FormControl("",
      [Validators.required,
        Validators.min(0),
        ,Validators.max(60)])
    });
  }
  resetForm() {
    this.formulario.reset(this.formulario.value);
  }
  guardar() {
    console.log(this.formulario)
    if(this.formulario.valid){
      this.controlForm=false
      this.vacuna.nombre = this.formulario.controls.nombre.value;
      this.vacuna.tipo = this.formulario.controls.tipo.value;
      this.vacuna.dosis = this.formulario.controls.dosis.value;
      this.vacuna.periodoEnDias = this.formulario.controls.periodoEnDias.value;
      this.vacunaServices.agregarVacuna(this.vacuna);
      this.mostrarNotificacion( "success", "Vacuna Registrada!",10000)
      this.cerrarModal();
    }else{
      this.controlForm=true
    }

  }

  cerrarModal() {
    this.modalService.dismissAll();

  }
}
