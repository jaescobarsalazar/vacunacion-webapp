import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { NbSearchService, NbToastrService, NbWindowService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Persona } from '../../../model/personas';
import { PersonasService } from '../../../services/personas.service';
import { Vacuna } from '../../../model/vacunas';
import { VacunasService } from '../../../services/vacunas.service';
@Component({
  selector: 'ngx-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {
  contador=0;
  vacunas: Vacuna[];
  value: any;
  vacunasFiltradas: Vacuna[];
  vacunasBusqueda: Vacuna[];
  edicion: boolean = false;
  vacunaEnEdicion: Vacuna;
  vacunaSeleccionada: Vacuna;
  index: number = 0;
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  submitted = false;
  closeResult: string;
  detalle: any;
  constructor(public vacunasServices: VacunasService,
    private windowService: NbWindowService,
    private searchService: NbSearchService,
    private toastrService: NbToastrService,
    private modalService: NgbModal,
  ) {
    this.searchService.onSearchSubmit()
      .subscribe((data: any) => {
        this.value = data.term;
        console.log("filtro")
        console.log(this.value)
      })

  }

  open(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {

    }, (reason) => {

    });
  }

  validarEliminar(vacuna, modal) {
    this.vacunaSeleccionada = vacuna;
    this.open(modal);
  }
  showToast(status, mensaje, duration
  ) {
    this.index += 1;
    this.toastrService.show(
      status || mensaje,
      ` ${mensaje}`,
      { status, duration });
  }

  cargarModificar(vacuna: Vacuna, modal) {
    //cargamos los datos de la vacuna a modificar
    this.vacunaSeleccionada = vacuna;
    //se abre la modal
    this.open(modal);
  }
  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Window content from template',
        context: {
          text: 'some text to pass into template',
        },
      },
    );
  }

  ngOnInit() {
    this.vacunasServices.obtenerVacunas().subscribe(datos => {
      this.vacunas = datos;
      //llamamos al servicio y obtenemos el listado de personas
   this.contador=this.vacunas.length;
    });
  }

  eliminar() {
      this.vacunasServices.borrarVacunas(this.vacunaSeleccionada);
      this.showToast('danger','Vacuna Eliminada',3000)
      this.cerrarModal();
     
  }

  cerrarModal(){
    this.modalService.dismissAll();
  }

  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }


}
