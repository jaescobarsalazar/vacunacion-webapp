import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VacunasRoutingModule } from './vacunas-routing.module';
import { AgregarComponent } from './agregar/agregar.component';
import { ListarComponent } from './listar/listar.component';
import { ModificarComponent } from './modificar/modificar.component';
import { VacunasComponent } from './vacunas.component';
import { NbCardModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AgregarComponent,
    ListarComponent,
    ModificarComponent,
    VacunasComponent
  ],
  imports: [
    CommonModule,
    VacunasRoutingModule,
    NbCardModule,
    ReactiveFormsModule
  ],
  entryComponents:[AgregarComponent,ModificarComponent]
})
export class VacunasModule { }
