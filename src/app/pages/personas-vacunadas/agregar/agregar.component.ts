import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormControl, FormGroup } from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService } from "@nebular/theme";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { PersonalSalud } from "../../../model/personal-salud";
import { Persona } from "../../../model/personas";
import { PersonasVacunadas } from "../../../model/personas-vacunadas";
import { Vacuna } from "../../../model/vacunas";
import { PersonalSaludService } from "../../../services/personal-salud.service";
import { PersonasVacunadasService } from "../../../services/personas-vacunadas.service";
import { PersonasService } from "../../../services/personas.service";
import { VacunasService } from "../../../services/vacunas.service";


@Component({
  selector: "ngx-agregar",
  templateUrl: "./agregar.component.html",
  styleUrls: ["./agregar.component.scss"],
})
export class AgregarComponent implements OnInit {
  personaSeleccionada: Persona;
  personasVacunadas: PersonasVacunadas[];
  listadoVacunas: Vacuna[];
  listadoPersonalSalud: PersonalSalud[];
  listadoPersonas: Persona[];
  controlBuscarNombre = false;
  formulario: FormGroup;
  personaVacunada: PersonasVacunadas = {
    id: "",
    personaVacunada: null,
    vacuna:null,
    profesionalSalud: null,
    fecha: null
  };

  constructor(
    public personasVacunadasServices: PersonasVacunadasService,
    public personasService: PersonasService,
    public vacunasServices: VacunasService,
    public personalSaludServices: PersonalSaludService,
    private fb: FormBuilder,
    public personaVacunadaServices: PersonasVacunadasService,
    private router: Router,
    private modalService: NgbModal,
    private toastrService: NbToastrService
  ) { }

  mostrarNotificacion(status, mensaje, duration
  ) {
    this.toastrService.show(
      status || mensaje,
      ` ${mensaje}`,
      { status, duration });
  }
  ngOnInit(): void {

    this.vacunasServices.obtenerVacunas().subscribe(datos => {
      this.listadoVacunas = datos;
      console.log(datos)
      console.log("vacunas cargadas")
    });
    this.personalSaludServices.obtenerPersonalSalud().subscribe(datos => {
      this.listadoPersonalSalud = datos;
      console.log("personal salud cargadas")
      console.log(datos)
    });

    this.personasService.obtenerPersonas().subscribe(datos => {
      this.listadoPersonas = datos;
      console.log(datos)
      console.log("personas cargadas")
    });

    this.formulario = this.fb.group({
      nombre: new FormControl(""),
      rut: new FormControl(""),
      vacuna: new FormControl(""),
      profesionalSalud: new FormControl(""),
      fecha: new FormControl(""),
    });
  }
  resetForm() {
    this.formulario.reset(this.formulario.value);
  }
  guardar() {
    var personaVacunada = this.listadoPersonas.find(x => x.rut == this.formulario.controls.rut.value);
    var vacunaSuministrada = this.listadoVacunas.find(x => x.id == this.formulario.controls.vacuna.value);
    var profesionalSalud = this.listadoPersonalSalud.find(x => x.id == this.formulario.controls.profesionalSalud.value);
    this.personaVacunada.personaVacunada = personaVacunada;
    this.personaVacunada.vacuna = vacunaSuministrada;
    this.personaVacunada.profesionalSalud = profesionalSalud;
    this.personaVacunada.fecha = this.formulario.controls.fecha.value;
    console.log(this.formulario.controls.profesionalSalud.value)
    console.log(this.formulario.controls.vacuna.value)
    this.personaVacunadaServices.agregarPersonaVacunada(this.personaVacunada);
    this.mostrarNotificacion("success", "Persona Vacunada Registrada!", 10000)
    this.cerrarModal();
  }

  buscarPorRut() {
    var test = this.listadoPersonas.find(x => x.rut === this.formulario.controls.rut.value);
    if (test) {
      this.controlBuscarNombre = false
      this.personaSeleccionada=test;
      this.formulario.controls.nombre.setValue(test.nombre);
    } else {
      this.personaSeleccionada=null;
      this.controlBuscarNombre = true
      this.formulario.controls.nombre.setValue('');
    }


  }

  cerrarModal() {
    this.modalService.dismissAll();

  }
}
