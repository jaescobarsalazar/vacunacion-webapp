import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { LocalDataSource } from 'ng2-smart-table';
import { Router } from '@angular/router';
import { NbSearchService, NbToastrService, NbWindowService } from '@nebular/theme';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Persona } from '../../../model/personas';
import { PersonasService } from '../../../services/personas.service';
import { Vacuna } from '../../../model/vacunas';
import { VacunasService } from '../../../services/vacunas.service';
import { PersonasVacunadas } from '../../../model/personas-vacunadas';
import { PersonasVacunadasService } from '../../../services/personas-vacunadas.service';
@Component({
  selector: 'ngx-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {
  personasVacunadas : PersonasVacunadas[] ;
  controlBusqueda=false;
  value:any;
  contador=0;
  personasVacunadasFiltradas : PersonasVacunadas[] ;
  personasVacunadasBusqueda : PersonasVacunadas[];
  edicion: boolean = false;
  personaVacunaEnEdicion: PersonasVacunadas;
  index : number = 0;
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  submitted = false;
  closeResult: string;
  detalle: any;
  constructor(public personasVacunadasServices: PersonasVacunadasService,
    private windowService: NbWindowService,
    private searchService: NbSearchService,
    private toastrService: NbToastrService,
    private modalService: NgbModal,
    ) {
      this.searchService.onSearchSubmit()
      .subscribe((data: any) => {
        this.value = data.term;
        console.log("filtro")
        console.log(this.value)
      })

  }


   open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {

    }, (reason) => {

    });
  }

  notificacionPladem(){
    this.showToast('','',10000);
  }
  showToast( status, mensaje,duration
    ) {
    status='danger'
    mensaje='Vacuna Registrada!'
    this.index += 1;
    this.toastrService.show(
      status || mensaje,
      ` ${mensaje}`,
      { status,duration });
  }
  openWindow(contentTemplate) {
    this.windowService.open(
      contentTemplate,
      {
        title: 'Window content from template',
        context: {
          text: 'some text to pass into template',
        },
      },
    );
  }

  ngOnInit() {
    this.personasVacunadasServices.obtenerPersonasVacunada().subscribe(datos => {
      this.personasVacunadas = datos;
      this.contador=this.personasVacunadas.length;
      //llamamos al servicio y obtenemos el listado de personas
    });
  }


  togleBusqueda(){
    this.controlBusqueda= !this.controlBusqueda;
  }

validarEliminar(vacuna, modal) {
  this.personaVacunaEnEdicion = vacuna;
  this.open(modal);
}

eliminar() {
    this.personasVacunadasServices.borrarPersonaVacunada(this.personaVacunaEnEdicion);
    this.showToast('danger','Vacuna Eliminada',3000)
    this.cerrarModal()
}
 AbrirModal(content, dato) {
    this.detalle = dato;
    this.modalService
      .open(content, { ariaLabelledBy: "modal-basic-title" })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  getDismissReason(reason: any) {
    throw new Error('Method not implemented.');
  }

  cerrarModal() {
    this.modalService.dismissAll();

  }

}
