import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonasVacunadasComponent } from './personas-vacunadas.component';

describe('PersonasVacunadasComponent', () => {
  let component: PersonasVacunadasComponent;
  let fixture: ComponentFixture<PersonasVacunadasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PersonasVacunadasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasVacunadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
