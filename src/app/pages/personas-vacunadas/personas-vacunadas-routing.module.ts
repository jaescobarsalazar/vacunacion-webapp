import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarComponent } from './listar/listar.component';
import { PersonasVacunadasComponent } from './personas-vacunadas.component';

const routes: Routes = [{
  path: '',
  component: PersonasVacunadasComponent,
  children: [
    {
      path: 'listar',
      component:ListarComponent,
    },
    {
      path: '',
      component:ListarComponent,
    },
  ],
}];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonasVacunadasRoutingModule { }
