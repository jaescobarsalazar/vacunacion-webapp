import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonasVacunadasRoutingModule } from './personas-vacunadas-routing.module';
import { ModificarComponent } from './modificar/modificar.component';
import { ListarComponent } from './listar/listar.component';
import { AgregarComponent } from './agregar/agregar.component';
import { PersonasVacunadasComponent } from './personas-vacunadas.component';
import { NbCardModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ModificarComponent,
    ListarComponent,
    AgregarComponent,
    PersonasVacunadasComponent
  ],
  imports: [
    CommonModule,
    PersonasVacunadasRoutingModule,
    NbCardModule,
    ReactiveFormsModule
  ],
  entryComponents:[AgregarComponent]
})
export class PersonasVacunadasModule { }
